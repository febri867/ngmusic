import { Stack, Typography, Button } from '@mui/material';
import { useNavigate } from 'react-router-dom';

const Page404 = () => {
  const navigate = useNavigate();
  return (
    <Stack sx={{ width: '100%', height: '100vh', justifyContent: 'center', alignItems: 'center' }}>
      <Typography component="h1" sx={{ fontFamily: 'Lato', fontSize: '24px' }}>
        404 Route Doesn't Exist
      </Typography>
      <Button
        variant="outlined"
        sx={{ marginTop: '10px', fontSize: '14px' }}
        onClick={() => navigate('/')}
      >
        Back to Home
      </Button>
    </Stack>
  );
};

export default Page404;
