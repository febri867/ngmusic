import { IEntityMusic } from '../../../../../domains/dashboard/entity';

export interface DashboardInitialState {
  musicList: IEntityMusic[];
  searchValue: string;
  count: number;
}
