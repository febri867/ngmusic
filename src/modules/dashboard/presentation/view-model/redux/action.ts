import { useDispatch } from 'react-redux';
import { usecase } from 'src/modules/dashboard/util';
import Helper from 'src/core/redux/helper';
import { Loading } from 'src/core/redux/types';

import { dashboardActions } from './slice';
import { IEntityMusic, IReqMusicList } from '../../../../../domains/dashboard/entity';
import { HTTPResponse } from '../../../../../domains/core/entity.ts';

const PaymentActionModel = () => {
  const dispatch = useDispatch();
  const { setSnackbarState, triggerLoading, setError } = Helper();

  function requestMusicList(payload: IReqMusicList): any {
    return async () => {
      try {
        triggerLoading({ actionType: Loading.overlay, loading: true });
        dispatch(dashboardActions.setSearchValue(payload.term));
        const response: HTTPResponse<IEntityMusic[]> = await usecase.requestMusicList(payload);
        if (response) {
          dispatch(dashboardActions.setMusicList(response.results));
          dispatch(dashboardActions.setCount(response.resultCount));
        }
      } catch (error) {
        // do something!
      } finally {
        triggerLoading({ actionType: Loading.overlay, loading: false });
      }
    };
  }

  return {
    requestMusicList: (payload: IReqMusicList) => dispatch(requestMusicList(payload)),
    setSnackbarState,
    triggerLoading,
    setError,
  };
};

export default PaymentActionModel;
