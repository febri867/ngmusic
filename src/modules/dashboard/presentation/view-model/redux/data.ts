import { useSelector } from 'react-redux';

import DataModel from 'src/core/redux/data';
import { RootState } from 'src/core/redux/store';
import { IEntityMusic } from '../../../../../domains/dashboard/entity';

export default function AuthDataModel() {
  const dataModel = DataModel();

  const musicList: IEntityMusic[] = useSelector((state: RootState) => state.dashboard.musicList);
  const searchValue: string = useSelector((state: RootState) => state.dashboard.searchValue);
  const count: string = useSelector((state: RootState) => state.dashboard.count);

  return {
    musicList,
    searchValue,
    count,
    ...dataModel,
  };
}
