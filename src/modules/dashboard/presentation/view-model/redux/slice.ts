import { createSlice } from '@reduxjs/toolkit';
import { DashboardInitialState } from './types';

const dashboardInitialState: DashboardInitialState = {
  musicList: [],
  searchValue: '',
  count: 0,
};

const dashboardSlice: any = createSlice({
  name: 'dashboard',
  initialState: dashboardInitialState,
  reducers: {
    setMusicList(state, { payload }) {
      state.musicList = payload;
    },
    setSearchValue(state, { payload }) {
      state.searchValue = payload;
    },
    setCount(state, { payload }) {
      state.count = payload;
    },
  },
});

export const dashboardActions = dashboardSlice.actions;

export default dashboardSlice;
