import { injectReducer } from 'src/core/redux/store';
import slice from './redux/slice';
import dataModel from './redux/data';
import actionModel from './redux/action';

export default function DashboardViewModel() {
  injectReducer('dashboard', slice.reducer);
  return {
    dataModel: () => dataModel(),
    actionModel: () => actionModel(),
  };
}
