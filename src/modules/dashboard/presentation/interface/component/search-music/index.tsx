import TextField from '../../../../../../shared/components/text-field';
import Button from '../../../../../../shared/components/button';
import React from 'react';
import DashboardViewModel from '../../../view-model';
import { SearchMusicStyle } from './style.ts';

interface ISearchMusic {
  finishSearch: () => void;
}
const SearchMusic = ({ finishSearch }: ISearchMusic) => {
  const { actionModel } = DashboardViewModel();
  const { requestMusicList } = actionModel();

  const [searchValue, setSearchValue] = React.useState<string>('');

  const handleOnChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const { value } = e.target;
    setSearchValue(value);
  };

  const onSearch = async () => {
    await requestMusicList({ term: searchValue, limit: 10 });
    finishSearch();
  };

  return (
    <SearchMusicStyle>
      {' '}
      <TextField
        stysle={{ marginBottom: '12px' }}
        id={'search-input'}
        onChange={handleOnChange}
        placeholder={'Artist / Album / Title'}
      />
      <Button
        onClick={onSearch}
        disabled={searchValue === ''}
        id={'search'}
        className={'btn-search'}
      >
        Search
      </Button>
    </SearchMusicStyle>
  );
};

export default SearchMusic;
