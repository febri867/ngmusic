import styled from '@emotion/styled';

export const SearchMusicStyle = styled.div`
  width: 100%;
  .btn-search {
    font-weight: 500;
    background-color: rgba(255, 255, 255, 0.2);
  }
`;
