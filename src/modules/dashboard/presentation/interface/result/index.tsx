import React from 'react';
import { useNavigate } from 'react-router-dom';
import {
  Text,
  TitleSmall,
  Title,
  Text2,
  TitleSmall3,
  TitleSmall2,
} from 'src/shared/components/typography';

import { ModalSearchStyle, ResultStyle } from './style';
import { Box } from '@mui/material';
import DashboardViewModel from '../../view-model';
import Button from '../../../../../shared/components/button';
import Modal from '@mui/material/Modal';
import SearchMusic from '../component/search-music';

const ResultInterface = () => {
  const navigate = useNavigate();
  const { actionModel, dataModel } = DashboardViewModel();
  const { requestMusicList } = actionModel();
  const { musicList, searchValue, count } = dataModel();
  const [modalSearch, setModalSearch] = React.useState<boolean>(false);
  let audio;
  const getCurrencySymbol = (currency) =>
    (0)
      .toLocaleString('en-US', {
        style: 'currency',
        currency,
        minimumFractionDigits: 0,
        maximumFractionDigits: 0,
      })
      .replace(/\d/g, '')
      .trim();

  const handlePlay = (src: string) => {
    if (audio) {
      audio.pause();
    }
    audio = new Audio(src);
    audio.play();
  };

  const handleLoadMore = () => {
    requestMusicList({ term: searchValue, limit: Number(count) + 10 });
  };

  const handleFinishSearch = () => {
    setModalSearch(false);
  };

  React.useEffect(() => {
    if (musicList.length === 0) {
      navigate('/');
    }
  });

  return (
    <ResultStyle>
      <div id="oval_parent">
        <div id="oval"></div>
      </div>
      <Box className={'bar'}>
        <img
          style={{ marginLeft: '12px' }}
          src={`menu.png`}
          alt="ngmusic"
          className="img-ngmusic"
        />
        <img src={`ngmusic.png`} alt="ngmusic" className="img-ngmusic" />
        <img
          style={{ marginRight: '12px', cursor: 'pointer' }}
          src={`search.png`}
          alt="ngmusic"
          className="img-ngmusic"
          onClick={() => setModalSearch(true)}
        />
      </Box>
      <Box className={'title-container'}>
        <TitleSmall style={{ fontWeight: 400 }}>Search result for :&nbsp;</TitleSmall>
        <Title style={{ color: '#7b34dd' }}>{searchValue}</Title>
      </Box>
      <Box display={'flex'} flexDirection={'column'} alignItems={'center'}>
        {musicList.map((i) => (
          <Box className={'list-container'} key={i.trackId}>
            <img src={i.artworkUrl100} alt="ngmusic" className="img-ngmusic" />
            <img
              onClick={() => handlePlay(i.previewUrl)}
              src={'play.png'}
              alt="ngmusic"
              className="play-ngmusic"
            />
            <Box sx={{ width: '100%', position: 'relative' }}>
              <Text2>{i.artistName}</Text2>
              <TitleSmall2>{i.trackName}</TitleSmall2>
              <Box className={'container-subtitle'}>
                <Text className={'genre-container'}>{i.primaryGenreName}</Text>
                <TitleSmall3 style={{ color: '#f5b014' }}>
                  <span className={'currency'}>{getCurrencySymbol(i.currency)}</span>
                  &nbsp;&nbsp;{i.trackPrice}
                </TitleSmall3>
              </Box>
            </Box>
          </Box>
        ))}
        <Button onClick={handleLoadMore} className={'btn-load-more'} id={'btn-load-more'}>
          Load More
        </Button>
      </Box>
      <Modal open={modalSearch}>
        <ModalSearchStyle>
          <Box width={'100%'} display={'flex'} flexDirection={'column'} alignItems={'center'}>
            <Title style={{ color: 'white', marginBottom: '24px', fontWeight: 400 }}>Search</Title>
            <SearchMusic finishSearch={handleFinishSearch} />
          </Box>
        </ModalSearchStyle>
      </Modal>
    </ResultStyle>
  );
};

export default ResultInterface;
