import styled from '@emotion/styled';

export const ResultStyle = styled.div`
  padding: 12px;
  min-height: 100vh;

  #oval_parent {
    position: absolute;
    top: 0;
    background: transparent;
    width: 100%;
    max-width: 426px;
    height: 80px;
    overflow: hidden;
  }

  #oval {
    width: calc(100% + 128px);
    height: 116px;
    margin: -50px 0 0 -64px;
    background: white;
    -moz-border-radius: 50%;
    -webkit-border-radius: 50%;
    box-shadow: 0 0 6px 0 rgba(148, 77, 230, 0.75);
    background-image: linear-gradient(100deg, #712bda, #a45deb 100%);
    border-radius: 50%;
  }

  .bar {
    display: flex;
    position: absolute;
    top: 8px;
    flex-direction: row;
    justify-content: space-between;
    width: 100%;
    padding: 8px;
    max-width: 400px;
  }

  .title-container {
    margin: 90px 0 20px 0;
    display: flex;
    flex-direction: row;
    justify-content: center;
    align-items: center;
  }

  .list-container {
    width: 100%;
    max-width: 426px;
    display: flex;
    flex-direction: row;
    box-shadow:
      0 4px 6px -1px rgba(0, 0, 0, 0.1),
      0 2px 4px -1px rgba(0, 0, 0, 0.06);
    background-color: #fff;
    padding: 12px 12px 11px 10px;
    border-radius: 10px;
    margin-bottom: 16px;
    position: relative;

    .img-ngmusic {
      border-radius: 10px;
      margin-right: 8px;
    }

    .play-ngmusic {
      position: absolute;
      margin: 34px;
      cursor: pointer;
    }

    .container-subtitle {
      display: flex;
      flex-direction: row;
      justify-content: space-between;
      position: absolute;
      bottom: 0;
      width: 100%;
    }

    .genre-container {
      border-radius: 20px;
      color: white;
      padding: 2px 12px;
      background-color: #10b981;
    }

    .currency {
      border: 4px solid #f5b014;
      border-radius: 100%;
      padding: 2px 7px;
    }
  }
  .btn-load-more {
    width: fit-content;
    background-color: #e2e8f0;
    color: #64748b;
  }
`;

export const ModalSearchStyle = styled.div`
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
  width: 100%;
  max-width: 400px;
  box-sizing: border-box;
  outline: none;
  display: flex;
  align-items: center;
  color: white;

  .btn-search {
    background-image: linear-gradient(98deg, #712bda, #a45deb);
  }
`;
