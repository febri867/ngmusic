import styled from '@emotion/styled';

export const HomeStyle = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  padding: 12px;
  flex-direction: column;
  background-image: linear-gradient(153deg, #712bda, #a45deb 100%);
  min-height: 100vh;

  .btn-search {
    font-weight: 500;
    background-color: rgba(255, 255, 255, 0.2);
  }
`;
