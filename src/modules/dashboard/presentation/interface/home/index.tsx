import { useNavigate } from 'react-router-dom';

import { HomeStyle } from './style';
import { Box } from '@mui/material';
import SearchMusic from '../component/search-music';

const HomeInterface = () => {
  const navigate = useNavigate();

  const handleFinishSearch = () => {
    navigate('/result');
  };

  return (
    <HomeStyle>
      <img src={`logo.png`} alt="ngmusic" className="img-ngmusic" />
      <Box
        sx={{ position: 'absolute', bottom: 0, maxWidth: '400px', width: '100%', padding: '24px' }}
      >
        <SearchMusic finishSearch={handleFinishSearch} />
      </Box>
    </HomeStyle>
  );
};

export default HomeInterface;
