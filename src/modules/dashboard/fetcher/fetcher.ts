import { Fetcher, FetcherHelper } from 'src/core/fetcher';
import { IEntityMusic, IReqMusicList } from '../../../domains/dashboard/entity';
import { parseToQueryString } from '../../../shared/utils/string';
import { HTTPResponse } from '../../../domains/core/entity.ts';

const { createAxios, clientErrorHandler, clientSuccessHandler } = FetcherHelper;

const baseApiUrl = import.meta.env.VITE_BASE_URL_API as string;

export default class DashboardFetcher extends Fetcher {
  constructor() {
    super();
    this.setUrl('');
    this.setQueryParam('');
    this.setData({});
    this.setSuccessHandler(clientSuccessHandler);
    this.setErrorHandler(clientErrorHandler);
  }

  setFetcherInstance(baseURL: string, headers: any, timeout: number = 30000) {
    const creation = {
      baseURL: baseURL,
      headers,
      timeout,
    };
    return this.setFetcher(createAxios(creation).instance);
  }

  async requestMusicList(payload: IReqMusicList) {
    try {
      this.setFetcherInstance(baseApiUrl, {});
      this.setUrl(`/search?${parseToQueryString(payload)}`);
      const response: HTTPResponse<IEntityMusic[]> = await this.get();
      return response;
    } catch (error) {
      throw error;
    }
  }
}
