import DashboardUsecase from 'src/domains/dashboard/usecase';
import DashboardFetcher from 'src/modules/dashboard/fetcher/fetcher';
import { IEntityMusic, IReqMusicList } from '../../../domains/dashboard/entity';
import { HTTPResponse } from '../../../domains/core/entity.ts';

export default class DashboardUseCaseImpl implements DashboardUsecase {
  private fetcher: DashboardFetcher;

  constructor(fetcher: DashboardFetcher) {
    this.fetcher = fetcher;
  }

  async requestMusicList(payload: IReqMusicList): Promise<HTTPResponse<IEntityMusic[]>> {
    try {
      return await this.fetcher.requestMusicList(payload);
    } catch (error) {
      throw error;
    }
  }
}
