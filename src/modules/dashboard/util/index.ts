import DashboardFetcher from '../fetcher/fetcher';
import DashboardUseCaseImpl from '../usecase/usecaseImpl';

export const fetcher = new DashboardFetcher();
export const usecase = new DashboardUseCaseImpl(fetcher);
