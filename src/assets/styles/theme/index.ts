import { createTheme } from '@mui/material/styles';

import palette from './palette';
import components from './components';

const theme = createTheme({
  palette,
  components,
});

export default theme;
