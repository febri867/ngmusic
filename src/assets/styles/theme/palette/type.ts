import {
  Palette as MuiPalette,
  PaletteOptions as MuiPaletteOptions,
  PaletteColor as MuiPaletteColor,
} from '@mui/material/styles';

interface PaletteColor extends MuiPaletteColor {
  main: string;
  contrast?: string;
  shade?: string;
  tint?: string;
  variant1?: string;
  variant2?: string;
  variant3?: string;
  variant4?: string;
  variant5?: string;
  variant6?: string;
  variant7?: string;
  variant8?: string;
  variant9?: string;
  variant10?: string;
  variant11?: string;
  variant12?: string;
  variant13?: string;
}

export interface PaletteOptions extends MuiPaletteOptions {
  primary: PaletteColor;
  secondary?: PaletteColor;
  tertiary?: PaletteColor;
  ashpalth?: PaletteColor;
  black?: PaletteColor;
  red?: PaletteColor;
  green?: PaletteColor;
  orange?: PaletteColor;
}

export interface Palette extends MuiPalette {
  primary: PaletteColor;
  secondary: PaletteColor;
  tertiary: PaletteColor;
  ashpalth: PaletteColor;
  black?: PaletteColor;
  red?: PaletteColor;
  green?: PaletteColor;
  orange?: PaletteColor;
}
