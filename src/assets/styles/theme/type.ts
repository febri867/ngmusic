import { Theme as MuiTheme, ThemeOptions as MuiThemeOptions } from '@mui/material/styles';

import { PaletteOptions, Palette } from 'src/assets/styles/theme/palette/type';

export interface ThemeOptions extends MuiThemeOptions {
  palette?: PaletteOptions;
}

export interface Theme extends MuiTheme {
  palette: Palette;
}
