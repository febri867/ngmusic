import { Components } from '@mui/material';

const components: Components = {
  // MuiInputBase: {
  //   styleOverrides: {
  //     root: {
  //       fontSize: '14px',
  //       fontWeight: '200',
  //     },
  //     input: {
  //       padding: '14.5px 14px !important',
  //     },
  //     inputSizeSmall: {
  //       padding: '8.5px 14px !important',
  //     },
  //   },
  // },
  MuiButton: {
    styleOverrides: {
      root: {
        textTransform: 'none',
        fontFamily: 'Roboto',
      },
    },
  },
};

export default components;
