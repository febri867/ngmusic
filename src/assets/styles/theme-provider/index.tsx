import { ThemeProvider as EmotionThemeProvider, ThemeProviderProps } from '@emotion/react';

import theme from '../theme';
import { Theme } from '../theme/type';

export type IThemeProvider = Pick<ThemeProviderProps, 'children'>;

const ThemeProvider = ({ children }: IThemeProvider) => {
  return <EmotionThemeProvider theme={theme as Theme}>{children}</EmotionThemeProvider>;
};

export default ThemeProvider;
