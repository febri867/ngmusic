import { Route } from 'src/domains/core/entity';
import { loadable } from 'src/shared/utils/loadable';

const HomePage = loadable(() => import('./index'));

const config: Route = {
  path: '/',
  key: 'home-page',
  component: <HomePage />,
};

export default config;
