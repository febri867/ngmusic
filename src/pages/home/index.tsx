import HomeInterface from 'src/modules/dashboard/presentation/interface/home';

const HomePage = () => {
  return (
    <>
      <HomeInterface />
    </>
  );
};

export default HomePage;
