import Page404Interface from 'src/modules/404/presentation/interface';

const Page404 = () => {
  return (
    <>
      <Page404Interface />
    </>
  );
};

export default Page404;
