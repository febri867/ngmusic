import { Route } from 'src/domains/core/entity';
import { loadable } from 'src/shared/utils/loadable';

const Page404 = loadable(() => import('./index'));

const config: Route = {
  path: '*',
  key: '404',
  component: <Page404 />,
};

export default config;
