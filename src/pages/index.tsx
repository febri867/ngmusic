import React from 'react';
import { Route, Routes, useLocation } from 'react-router-dom';

import { Route as IRoute } from 'src/domains/core/entity';

import Page404 from './404/loadable';
import HomePage from './home/loadable';
import ResultPage from './result/loadable.tsx';

const generateRoutes = (pages: IRoute[]) => {
  return (
    <Routes>
      {pages.map(({ key, path, component }) => (
        <Route key={key} path={path} element={component} />
      ))}
    </Routes>
  );
};

export const Unauthenticated = () => {
  return <>{generateRoutes([Page404, HomePage, ResultPage])}</>;
};

export const Authenticated = () => {
  return <>{generateRoutes([Page404])}</>;
};

export const Pages = () => {
  const { pathname } = useLocation();

  // Disable scrolling on html input type
  React.useEffect(() => {
    document.addEventListener('wheel', function () {
      if ((document?.activeElement as HTMLInputElement)?.type === 'number') {
        (document?.activeElement as HTMLInputElement)?.blur();
      }
    });
  }, []);

  React.useEffect(() => {
    // "document.documentElement.scrollTo" is the magic for React Router Dom v6
    document.documentElement.scrollTo({
      top: 0,
      left: 0,
    });
  }, [pathname]);

  return <Unauthenticated />;
};
