import ResultInterface from 'src/modules/dashboard/presentation/interface/result';

const ResultPage = () => {
  return (
    <>
      <ResultInterface />
    </>
  );
};

export default ResultPage;
