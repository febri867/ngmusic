import { Route } from 'src/domains/core/entity';
import { loadable } from 'src/shared/utils/loadable';

const ResultPage = loadable(() => import('./index'));

const config: Route = {
  path: '/result',
  key: 'result-page',
  component: <ResultPage />,
};

export default config;
