import { BrowserRouter } from 'react-router-dom';
import { Provider as ReduxProvider } from 'react-redux';

import { Pages } from './pages';
import { WrapperFrame } from './shared/components/wrapper';
import { store } from 'src/core/redux/store';
import OverlayLoading from 'src/shared/components/overlay-loading';
import ErrorPopup from 'src/shared/components/error-popup';

function App() {
  return (
    <ReduxProvider store={store}>
      <WrapperFrame>
        <BrowserRouter>
          <ErrorPopup />
          <OverlayLoading />
          <Pages />
        </BrowserRouter>
      </WrapperFrame>
    </ReduxProvider>
  );
}

export default App;
