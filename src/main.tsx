import ReactDOM from 'react-dom/client';
import { ThemeProvider } from '@emotion/react';

import 'normalize.css';

import theme from 'src/assets/styles/theme/index';
import { Theme } from 'src/assets/styles/theme/type';

import App from './App.tsx';

ReactDOM.createRoot(document.getElementById('root')!).render(
  <ThemeProvider theme={theme as Theme}>
    <App />
  </ThemeProvider>,
);
