export interface IEntityMusic {
  trackId: number;
  artistName: string;
  trackName: string;
  primaryGenreName: string;
  currency: string;
  trackPrice: number;
  artworkUrl100: string;
  previewUrl: string;
}

export interface IReqMusicList {
  term: string;
  limit: number;
}
