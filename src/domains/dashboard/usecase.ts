import { IEntityMusic, IReqMusicList } from './entity';
import { HTTPResponse } from '../core/entity.ts';

export default interface DashboardUsecase {
  requestMusicList: (payload: IReqMusicList) => Promise<HTTPResponse<IEntityMusic[]>>;
}
