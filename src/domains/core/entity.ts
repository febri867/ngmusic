export interface Route {
  path: string;
  key: string;
  component: any;
}

export interface HTTPResponse<D> {
  resultCount?: string;
  results?: D;
}
