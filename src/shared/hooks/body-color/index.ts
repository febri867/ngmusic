import React from 'react';

interface Props {
  color: string;
}

const useBodyColor = ({ color }: Props) => {
  React.useEffect(() => {
    document.body.style.background = color;
    return () => {
      document.body.style.background = 'none';
    };
  }, [color]);
  return true;
};

export default useBodyColor;
