import styled from '@emotion/styled';

export const Title = styled.p`
  font-family: Roboto;
  font-size: 20px;
  font-weight: 900;
  line-height: 26px;
  letter-spacing: 0.02em;
  text-align: left;
  margin: 0px 0px 5px 0px;
  color: ${({
    theme: {
      palette: { black },
    },
  }) => black?.variant1};
`;

export const TitleSmall = styled.p`
  font-family: Roboto;
  font-size: 16px;
  font-weight: 700;
  line-height: 21.12px;
  letter-spacing: 0.02em;
  text-align: left;
  margin: 0px 0px 5px 0px;
  color: ${({
    theme: {
      palette: { black },
    },
  }) => black?.variant4};
`;

export const TitleSmall2 = styled.p`
  font-family: Roboto;
  font-size: 16px;
  font-weight: 700;
  line-height: 21.12px;
  letter-spacing: 0.02em;
  text-align: left;
  margin: 0px 0px 5px 0px;
  color: ${({
    theme: {
      palette: { black },
    },
  }) => black?.variant1};
`;

export const TitleSmall3 = styled.p`
  font-family: Roboto;
  font-size: 16px;
  font-weight: 700;
  line-height: 21.12px;
  letter-spacing: 0.02em;
  text-align: left;
  margin: 0px 0px 5px 0px;
  color: ${({
    theme: {
      palette: { primary },
    },
  }) => primary?.main};
`;

export const Text = styled.p`
  font-family: Roboto;
  font-size: 14px;
  font-weight: 400;
  line-height: 20px;
  letter-spacing: 0.01em;
  text-align: left;
  margin: 0px 0px 5px 0px;
  color: ${({
    theme: {
      palette: { black },
    },
  }) => black?.variant2};
`;

export const Text1 = styled.p`
  font-family: Roboto;
  font-size: 14px;
  font-weight: 900;
  line-height: 20px;
  letter-spacing: 0.01em;
  text-align: left;
  margin: 0px 0px 5px 0px;
  color: ${({
    theme: {
      palette: { black },
    },
  }) => black?.variant1};
`;

export const Text2 = styled.p`
  font-family: Roboto;
  font-size: 14px;
  font-weight: 400;
  line-height: 20px;
  letter-spacing: 0.01em;
  text-align: left;
  margin: 0px 0px 5px 0px;
  color: ${({
    theme: {
      palette: { black },
    },
  }) => black?.variant4};
`;

export const Text3 = styled.p`
  font-family: Roboto;
  font-size: 14px;
  font-weight: 400;
  line-height: 20px;
  letter-spacing: 0.01em;
  text-align: left;
  margin: 0px 0px 5px 0px;
  color: ${({
    theme: {
      palette: { ashpalth },
    },
  }) => ashpalth?.variant8};
`;

export const Text4 = styled.p`
  font-family: Roboto;
  font-size: 14px;
  font-weight: 400;
  line-height: 20px;
  letter-spacing: 0.01em;
  text-align: left;
  margin: 0px 0px 5px 0px;
  color: ${({
    theme: {
      palette: { orange },
    },
  }) => orange?.main};
`;

export const Text5 = styled.p`
  font-family: Roboto;
  font-size: 14px;
  font-weight: 900;
  line-height: 20px;
  letter-spacing: 0.01em;
  text-align: left;
  margin: 0px 0px 5px 0px;
  color: ${({
    theme: {
      palette: { black },
    },
  }) => black?.variant12};
`;

export const BlackText = styled.p`
  font-family: Roboto;
  font-size: 14px;
  font-weight: 400;
  line-height: 20px;
  letter-spacing: 0.01em;
  text-align: left;
  margin: 0px 0px 5px 0px;
  color: ${({
    theme: {
      palette: { tertiary },
    },
  }) => tertiary?.main};
`;

export const TextSmall = styled.p`
  font-family: Roboto;
  font-size: 12px;
  font-weight: 400;
  line-height: 12px;
  letter-spacing: 0.15000000596046448px;
  text-align: left;
  margin: 0px 0px 5px 0px;
  color: ${({
    theme: {
      palette: { black },
    },
  }) => black?.variant1};
`;

export const TextSmall1 = styled.p`
  font-family: Roboto;
  font-size: 12px;
  font-weight: 400;
  line-height: 15.84px;
  letter-spacing: 0.15000000596046448px;
  text-align: left;
  margin: 0px 0px 5px 0px;
  color: ${({
    theme: {
      palette: { black },
    },
  }) => black?.variant1};
`;

export const TextSmall2 = styled.p`
  font-family: Roboto;
  font-size: 12px;
  font-weight: 400;
  line-height: 15.84px;
  letter-spacing: 0.15000000596046448px;
  text-align: left;
  margin: 0px 0px 5px 0px;
  color: ${({
    theme: {
      palette: { black },
    },
  }) => black?.variant2};
`;

export const TextSmall3 = styled.p`
  font-family: Roboto;
  font-size: 12px;
  font-weight: 700;
  line-height: 15.84px;
  letter-spacing: 0.15000000596046448px;
  text-align: left;
  margin: 0px 0px 5px 0px;
  color: ${({
    theme: {
      palette: { common },
    },
  }) => common?.white};
`;

export const TextSmall4 = styled.p`
  font-family: Roboto;
  font-size: 12px;
  font-weight: 700;
  line-height: 14px;
  letter-spacing: 0.15000000596046448px;
  text-align: left;
  margin: 0px 0px 5px 0px;
  color: ${({
    theme: {
      palette: { secondary },
    },
  }) => secondary?.main};
`;

export const SecondaryTextSmall = styled.p`
  font-family: Roboto;
  font-size: 12px;
  font-weight: 400;
  line-height: 20px;
  letter-spacing: 0.01em;
  text-align: left;
  margin: 0px 0px 5px 0px;
  color: ${({
    theme: {
      palette: { secondary },
    },
  }) => secondary?.main};
`;

export const VerySmallText = styled.p`
  font-family: Roboto;
  font-size: 8px;
  font-weight: 400;
  line-height: 12px;
  letter-spacing: 0.15000000596046448px;
  text-align: left;
  margin: 0px 0px 5px 0px;
  color: ${({
    theme: {
      palette: { tertiary },
    },
  }) => tertiary?.main};
`;

export const VerySmallText2 = styled.p`
  font-family: Roboto;
  font-size: 8px;
  font-weight: 500;
  line-height: 8px;
  letter-spacing: 0.15000000596046448px;
  text-align: left;
  margin: 0px 0px 5px 0px;
  color: ${({
    theme: {
      palette: { ashpalth },
    },
  }) => ashpalth?.variant8};
`;

export const VerySmallText3 = styled.p`
  font-family: Roboto;
  font-size: 8px;
  font-weight: 700;
  line-height: 8px;
  letter-spacing: 0.15000000596046448px;
  text-align: left;
  margin: 0px 0px 5px 0px;
  color: ${({
    theme: {
      palette: { red },
    },
  }) => red?.variant2};
`;

export const VerySmallLinkText = styled.p`
  font-family: Roboto;
  font-size: 8px;
  font-weight: 400;
  line-height: 12px;
  letter-spacing: 0.15000000596046448px;
  text-align: left;
  margin: 0px 0px 5px 0px;
  text-decoration: underline;
  text-decoration-color: ${({
    theme: {
      palette: { secondary },
    },
  }) => secondary?.main};
  color: ${({
    theme: {
      palette: { secondary },
    },
  }) => secondary?.main};
`;

export const BigText = styled.p`
  font-family: Roboto;
  font-size: 59px;
  font-weight: 700;
  line-height: 12px;
  letter-spacing: 0.15000000596046448px;
  text-align: left;
  margin: 0px 0px 5px 0px;
  padding: 15px 0;
  box-sizing: border-box;
  color: ${({
    theme: {
      palette: { secondary },
    },
  }) => secondary?.variant1};
`;
