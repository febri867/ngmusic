import styled from '@emotion/styled';

export const LinearProgressStyle = styled.div`
  padding: 0px 15px;
`;
