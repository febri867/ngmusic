import LinearProgress from '@mui/material/LinearProgress';

import { LinearProgressStyle } from './style';

interface Props {
  value: number;
}

const ProgressBar = ({ value = 0 }: Props) => {
  return (
    <LinearProgressStyle>
      <LinearProgress
        variant="determinate"
        value={value}
        sx={{
          height: '18px',
          borderRadius: '5px',
          backgroundColor: 'ashpalth.variant1',
        }}
      />
    </LinearProgressStyle>
  );
};

export default ProgressBar;
