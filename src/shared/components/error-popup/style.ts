import styled from '@emotion/styled';

export const ErrorPopupStyle = styled.div`
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
  width: 90%;
  max-width: 450px;
  background-color: white;
  box-sizing: border-box;
  padding: 20px;
  outline: none;
  display: flex;
  flex-direction: column;
  align-items: center;
  text-align: center;
  border-radius: 10px;

  .title {
    margin-bottom: 15px;
    p {
      text-align: center;
    }
  }

  .title-style1 {
    margin-bottom: 15px;
  }

  .text-title-style1 {
    text-align: center;
  }

  .message {
    margin-bottom: 25px;
    line-height: 18px;
    color: ${({
      theme: {
        palette: { red },
      },
    }) => red?.variant1};
  }

  .message-style1 {
    margin-bottom: 25px;
    font-size: 16px;
    font-weight: 400;
    line-height: 18px;
    color: ${({
      theme: {
        palette: { black },
      },
    }) => black?.variant1};
  }

  .image-wrapper {
    margin-bottom: 1rem;
  }

  .action {
    width: 100%;
  }
`;
