import React from 'react';
import Modal from '@mui/material/Modal';
import { useLocation } from 'react-router-dom';

import DataModel from 'src/core/redux/data';
import { ErrorPopupState } from 'src/core/redux/types';

import { ErrorPopupStyle } from './style';
import Button from '../button';
import { Title } from '../typography';
import Helper from 'src/core/redux/helper';

const ErrorPopup = () => {
  const { setErrorPopupState } = Helper();
  const { errorPopupState } = DataModel();

  const { pathname } = useLocation();

  const eps = errorPopupState as ErrorPopupState;

  const titleCn = eps.popUpStyle ? `title-${eps.popUpStyle}` : 'title';
  const textTitleCn = eps.popUpStyle ? `text-title-${eps.popUpStyle}` : '';
  const messageCn = eps.popUpStyle ? `message-${eps.popUpStyle}` : 'message';

  const onCloseErrorPopup = React.useCallback(() => {
    setErrorPopupState({
      ...eps,
      open: false,
    });
    if (eps?.callback) {
      eps.callback();
    }
  }, [eps]);

  React.useEffect(() => {
    onCloseErrorPopup();
  }, [pathname]);

  const action = React.useMemo(() => {
    if (eps?.action) {
      return eps?.action.map(({ id, cta }) => (
        <Button
          key={id}
          id={id as string}
          type="button"
          onClick={id === 'btn-error-close' ? onCloseErrorPopup : null}
        >
          {cta}
        </Button>
      ));
    }
    return null;
  }, [eps?.action]);

  const isNikRegistered = React.useMemo(() => {
    if (eps?.errorCode === 'UPLOAD_KTP_40005') {
      return true;
    }
    return false;
  }, [eps?.errorCode]);

  return (
    <>
      <Modal open={eps?.open || false}>
        <ErrorPopupStyle>
          <div className={titleCn}>
            <Title className={textTitleCn}>{eps?.title}</Title>
          </div>
          {isNikRegistered ? (
            <div className="image-wrapper">
              <img src="/images/ktp-not-found.webp" alt="ktp not found" />
            </div>
          ) : (
            <div className={messageCn}>{eps?.message}</div>
          )}
          <div className="action">{action}</div>
        </ErrorPopupStyle>
      </Modal>
    </>
  );
};

export default ErrorPopup;
