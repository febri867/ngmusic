import MUIButton from '@mui/material/Button';
import { ButtonBaseProps } from '@mui/material';

interface ButtonProps {
  id: string;
  children?: any;
  size?: 'large' | 'medium' | 'small';
  sx?: ButtonBaseProps['sx'];
  type?: 'button' | 'submit';
  disabled?: boolean;
  onClick?: any;
  variant?: 'contained' | 'outlined' | 'text';
  href?: string;
  className?: string;
}

const Button = ({
  id,
  children,
  size = 'large',
  type = 'button',
  sx = {},
  disabled = false,
  onClick,
  variant = 'contained',
  href,
  className,
}: ButtonProps) => {
  return (
    <MUIButton
      id={id}
      variant={variant}
      size={size}
      type={type}
      sx={{ fontSize: '14px', borderRadius: '20px', ...sx }}
      disabled={disabled}
      onClick={onClick}
      href={href}
      fullWidth
      className={className}
    >
      {children}
    </MUIButton>
  );
};

export default Button;
