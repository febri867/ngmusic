import styled from '@emotion/styled';

export const OverlayLoadingStyle = styled.div`
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
  width: 90%;
  max-width: 300px;
  box-sizing: border-box;
  padding: 15px;
  outline: none;
  display: flex;
  align-items: center;
`;
