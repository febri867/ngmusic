import Modal from '@mui/material/Modal';
import CircularProgress from '@mui/material/CircularProgress';

import DataModel from 'src/core/redux/data';

import { OverlayLoadingStyle } from './style';

export default function OverlayLoading() {
  const { overlayLoading, overlayKtpUploadLoading } = DataModel();

  return (
    <>
      <Modal open={overlayLoading || overlayKtpUploadLoading || false}>
        <OverlayLoadingStyle>
          <div>
            <CircularProgress size={35} thickness={4} sx={{ marginRight: '15px' }} />
          </div>
          {overlayLoading && <p>Permintaan sedang diproses...</p>}
          {overlayKtpUploadLoading && (
            <p>Sedang melakukan penyesuaian ukuran pada foto, mohon menunggu beberapa saat...</p>
          )}
        </OverlayLoadingStyle>
      </Modal>
    </>
  );
}
