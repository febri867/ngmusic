import styled from '@emotion/styled';

export const WrapperFrame = styled.div`
  max-width: 450px;
  min-height: 100vh;
  margin: 0 auto;
  padding: 0;
  box-sizing: border-box;
`;
