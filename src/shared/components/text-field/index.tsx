import React from 'react';
import MUITextField from '@mui/material/TextField';
import InputAdornment from '@mui/material/InputAdornment';
import MenuItem from '@mui/material/MenuItem';
import Visibility from '@mui/icons-material/Visibility';
import VisibilityOff from '@mui/icons-material/VisibilityOff';
import ButtonBase from '@mui/material/ButtonBase';

interface Option {
  id: number | string;
  code?: string;
  name: string;
}

interface TextFieldProps {
  id: string;
  label: string;
  placeholder?: string;
  name?: string;
  onChange?: any;
  onBlur?: any;
  error?: boolean;
  helperText?: string;
  onClick?: any;
  endAdornment?: any;
  value?: any;
  select?: boolean;
  options?: Option[];
  autoComplete?: string;
  type?: 'text' | 'password' | 'number';
  disabled?: boolean;
  ref?: any;
}

const TextField = React.forwardRef(
  (
    {
      id,
      placeholder,
      label,
      name,
      onChange,
      onBlur,
      error = false,
      helperText,
      onClick,
      endAdornment,
      value,
      select = false,
      options = [],
      autoComplete,
      type = 'text',
      disabled = false,
      ...props
    }: TextFieldProps,
    ref,
  ) => {
    const [showPassword, setShowPassword] = React.useState(false);

    const EndAdornment = React.useCallback(() => {
      if (type === 'password' && showPassword && !endAdornment) {
        return (
          <InputAdornment position="end">
            <ButtonBase onClick={() => setShowPassword(false)}>
              <VisibilityOff />
            </ButtonBase>
          </InputAdornment>
        );
      } else if (type === 'password' && !showPassword && !endAdornment) {
        return (
          <InputAdornment position="end">
            <ButtonBase onClick={() => setShowPassword(true)}>
              <Visibility />
            </ButtonBase>
          </InputAdornment>
        );
      } else if (endAdornment) {
        return <InputAdornment position="end">{endAdornment}</InputAdornment>;
      }
      return null;
    }, [type, showPassword, endAdornment]);

    const inputType = React.useMemo(() => {
      if (showPassword && type === 'password') {
        return 'text';
      } else if (!showPassword && type === 'password') {
        return 'password';
      }
      return type;
    }, [showPassword, type]);

    return (
      <MUITextField
        // ref={React.forwardRef(ref)}
        ref={ref}
        id={id}
        label={label}
        type={inputType}
        variant="filled"
        placeholder={placeholder}
        size="medium"
        fullWidth
        name={name}
        onChange={onChange}
        onBlur={onBlur}
        helperText={error && helperText}
        onClick={onClick}
        value={value}
        data-lol="lol"
        sx={{
          borderRadius: '20px',
          paddingLeft: '0px',
          backgroundColor: 'white',
          border: 'none !important',
          label: {
            fontFamily: 'Roboto',
            fontSize: '14px',
            fontWeight: 700,
            lineHeight: '20px',
            transform: 'none',
            color: !error ? 'black.variant3' : 'red.variant1',
            '&.Mui-focused': {
              color: !error ? 'black.variant3' : 'red.variant1',
            },
          },
          'input, .MuiSelect-standard, .MuiAutocomplete-input': {
            borderRadius: '20px',
            fontFamily: 'Roboto',
            fontSize: '14px',
            fontWeight: 400,
            lineHeight: '14px',
            transform: 'none',
            color: !error ? 'black.variant3' : 'red.variant1',
            textTransform: 'none',
            paddingTop: '12px !important',
          },
          'input::placeholder': {
            textAlign: 'center',
            color: '#64748b',
            fontSize: '12px',
            fontWeight: 500,
            opacity: 1,
            letterSpacing: '0.43px',
            fontFamily: 'Roboto',
          },
          'label[data-shrink=false]+.MuiInputBase-formControl input': {
            '&::-webkit-input-placeholder': {
              opacity: '0.42 !important',
            },
          },
          'input::-webkit-outer-spin-button, input::-webkit-inner-spin-button': {
            MozAppearance: 'none',
            WebkitAppearance: 'none',
            margin: '0',
          },
          'input[type=number]': {
            MozAppearance: 'textfield',
          },
          '.Mui-focused': {
            borderRadius: '20px',
            color: !error ? 'black.variant3' : 'red.variant1',
          },
          '.MuiInputBase-root, .MuiInputBase-root:before': {
            borderBottom: 'none !important',
            borderRadius: '20px',
            borderColor: !error ? 'ashpalth.variant3' : 'red.variant1',
          },
          '.MuiInput-underline:after': {
            border: 'none !important',
          },
          '.MuiInput-underline:before': {
            border: 'none !important',
          },
          '.MuiFormHelperText-root': {
            fontFamily: 'Roboto',
            color: !error ? 'ashpalth.variant3' : 'red.variant1',
          },
          '.select-placeholder': {
            fontSize: '12px',
            fontWeight: 500,
          },
        }}
        InputLabelProps={{
          shrink: true,
        }}
        InputProps={{
          endAdornment: <EndAdornment />,
          autoComplete,
        }}
        select={select}
        SelectProps={{
          renderValue: (selected) => {
            if (!selected || (selected as any).length === 0) {
              return <div className="select-placeholder">{placeholder}</div>;
            }
            if (options.length === 0) {
              return '';
            }
            return options?.[options?.findIndex((option) => option.id === value)].name;
          },
          displayEmpty: true,
        }}
        disabled={disabled}
        onKeyDown={
          type === 'number'
            ? (e) => ['e', 'E', '+', '-', ',', '.'].includes(e.key) && e.preventDefault()
            : undefined
        }
        {...props}
      >
        {select &&
          options &&
          options.map((option) => (
            <MenuItem
              key={option.id}
              value={option.id}
              sx={{ fontFamily: 'Lato', fontSize: '14px', fontWeight: 400 }}
            >
              {option.name}
            </MenuItem>
          ))}
      </MUITextField>
    );
  },
);

export default TextField;

// css-1x51dt5-MuiInputBase-input-MuiInput-input
// css-1x51dt5-MuiInputBase-input-MuiInput-input
