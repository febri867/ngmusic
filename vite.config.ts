import { defineConfig } from 'vite';
import react from '@vitejs/plugin-react';
import path from 'path';

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [react()],
  build: {
    modulePreload: {
      resolveDependencies: (url, deps, context) => {
        return [];
      },
    },
    rollupOptions: {
      output: {
        sourcemap: false,
        manualChunks: {
          'react-dom': ['react-dom'],
          'react-router': ['react-router-dom'],
          rtk: ['@reduxjs/toolkit'],
          'react-redux': ['react-redux'],
          axios: ['axios'],
        },
      },
    },
  },
  resolve: {
    alias: {
      src: path.resolve('src/'),
    },
  },
  server: {
    port: 3000,
  },
});
